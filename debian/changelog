libiscwt-java (5.5.20131203+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Point Homepage to webarchive
  * Drop README.Debian since not really informative
  * d/copyright:
     - DEP5
     - Files-Excluded
  * d/orig-tar.sh
  * Fix watch file
  * Build-Depends: s/default-jdk/default-jdk-headless/
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Drop useless get-orig-source target (routine-update)
  * Trim trailing whitespace.
  * Use secure URI in debian/watch.

 -- Andreas Tille <tille@debian.org>  Tue, 04 Feb 2025 22:30:12 +0100

libiscwt-java (5.3.20100629-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 15:48:17 +0100

libiscwt-java (5.3.20100629-4) unstable; urgency=medium

  * Team upload.
  * Transition to libswt-gtk-4-java
  * Moved the package to Git
  * Standards-Version updated to 3.9.8
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 01 Aug 2016 23:32:53 +0200

libiscwt-java (5.3.20100629-3) unstable; urgency=low

  [ Miguel Landaeta ]
  * Team upload.
  * Bump Standards-Version to 3.9.4. No changes were required.
  * Fix vcs-field-not-canonical lintian warning.
  * Fix extended-description-is-probably-too-short lintian warning.
  * Fix needless-dependency-on-jre lintian warning.

  [ James Page ]
  * Fix FTBFS with openjdk-7 as default-jdk (LP: #888963) (Closes: 678032):
    - d/patches/java7-compat.patch: Update usage of FontManager for
      Java 7.

  [ ShuxiongYe ]
  * Compatible with both openjdk-6 and openjdk-7
    - debian/patch/java6andjava7-compat.patch

 -- ShuxiongYe <yeshuxiong@gmail.com>  Fri, 05 Jul 2013 18:59:40 +0800

libiscwt-java (5.3.20100629-2) unstable; urgency=low

  [ Miguel Landaeta ]
  * Team upload.
  * Use public access URL in Vcs-Svn field. (Closes: #670276).
  * Bump Standards-Version to 3.9.3. No changes were required.

  [ Niels Thykier ]
  * Updated description.
  * Use swt.jar rather than swt-gtk-3.7.jar.  (Closes: #670915)
  * Rebuild in sid environment.  (Closes: #663396)

 -- Niels Thykier <niels@thykier.net>  Mon, 30 Apr 2012 15:07:59 +0200

libiscwt-java (5.3.20100629-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Switch to source format 3.0.
  * Fix Build-Depends. (Closes: #629736)
  * Update Standards-Version: 3.9.2.
  * Implement get-orig-source and drop javadoc from orig-tarball.
  * Switch to javahelper.
  * Drop versioned binary package.

 -- Torsten Werner <twerner@debian.org>  Sun, 04 Sep 2011 01:23:11 +0200

libiscwt-java (5.2.20091102-2) unstable; urgency=low

  * Fix build dependencies (Closes: #573682)
    Many thanks go to Lucas, Hideki and Torsten
    for their support.
  * Added debian/README.Debian.
  * Cleaned up empty files in debian folder.

 -- Steffen Moeller <moeller@debian.org>  Fri, 23 Apr 2010 23:29:54 +0200

libiscwt-java (5.2.20091102-1) unstable; urgency=low

  * Initial release (Closes: #565695).

 -- Steffen Moeller <moeller@debian.org>  Mon, 18 Jan 2010 00:29:38 +0100
